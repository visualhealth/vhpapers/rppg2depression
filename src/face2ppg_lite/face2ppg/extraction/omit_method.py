from scipy import signal
import numpy as np


# Function inspired by https://github.com/phuselab/pyVHR

class omit_method:
    """ This method is described in the following paper:
        "Face2PPG: Towards a reliable and unobtrusive blood volume pulse extraction from faces using RGB cameras"
        by Álvarez Casado, C and Bordallo López, M
    """
    methodName = 'OMIT'

    def __init__(self, fps):
        self.frameRate = fps

    def apply(self, X):
        bvp = []
        for i in range(signal.shape[0]):
            X = signal[i]
            Q, R = np.linalg.qr(X)
            S = Q[:, 0].reshape(1, -1)
            P = np.identity(3) - np.matmul(S.T, S)
            Y = np.dot(P, X)
            bvp.append(Y[1, :])
        bvp = np.array(bvp)
        return

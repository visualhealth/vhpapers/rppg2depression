from scipy import signal
import numpy as np

# Function inspired by https://github.com/phuselab/pyVHR

class lgi_method:
    """ This method is described in the following paper:
        "Face2PPG: Towards a reliable and unobtrusive blood volume pulse extraction from faces using RGB cameras"
        by Álvarez Casado, C and Bordallo López, M
    """
    methodName = 'LGI'
    projection = np.array([[1, 0, -1], [0, 1, 0], [-1, 0, 1]])

    def __init__(self, fps):
        self.frameRate = fps


    def apply(self, X):

        U, _, _ = np.linalg.svd(X)

        S = U[:, 0].reshape(1, -1)  # array 2D shape (1,3)
        P = np.identity(3) - np.matmul(S.T, S)

        Y = np.dot(P, X)
        bvp = Y[1, :]

        return bvp
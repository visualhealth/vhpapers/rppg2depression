import os
import sys
from os.path import join

import cv2
import numpy as np
import pandas as pd

sys.path.insert(0, os.path.abspath('..'))

from face2ppg.face.face_detection import FaceDetector
from face2ppg.face.face_landmarks_detection import FaceLandmarksDetector
import glob
from datetime import datetime
from face2ppg.segmentation.dnn_skin_detection import skinDetector
from face2ppg.plotting.VHRTPlotter import RT2DPlotter
from scipy import signal
from face2ppg.helpers.common import *
import face2ppg.analysis.spectral_analysis as sa
from face2ppg.extraction.pos_method import pos_method
from face2ppg.extraction.chrom_method import chrom_method
from face2ppg.extraction.lgi_method import lgi_method
from face2ppg.extraction.omit_method import omit_method
from face2ppg.filtering.filtering import Filtering
import ffmpeg


def check_rotation(path_video_file):
    # this returns meta-data of the video file in form of a dictionary
    meta_dict = ffmpeg.probe(path_video_file)

    # from the dictionary, meta_dict['streams'][0]['tags']['rotate'] is the key
    # we are looking for
    # print(meta_dict['streams'][0]['tags'])

    rotateCode = None
    if 'tags' in meta_dict['streams'][0]:
        if 'rotate' in meta_dict['streams'][0]['tags']:
            # print("YES")
            if int(meta_dict['streams'][0]['tags']['rotate']) == -90:
                # print(f" Rotate 1: {int(meta_dict['streams'][0]['tags']['rotate'])}")
                rotateCode = cv2.ROTATE_180
            elif int(meta_dict['streams'][0]['tags']['rotate']) == 180:
                # print(f" Rotate 2: {int(meta_dict['streams'][0]['tags']['rotate'])}")
                # rotateCode = cv2.ROTATE_180
                rotateCode = cv2.ROTATE_90_CLOCKWISE
            elif int(meta_dict['streams'][0]['tags']['rotate']) == 270:
                # print(f" Rotate 3: {int(meta_dict['streams'][0]['tags']['rotate'])}")
                # rotateCode = cv2.ROTATE_90_COUNTERCLOCKWISE
                rotateCode = cv2.ROTATE_180

            print(f"    >> Video Rotation needed: {meta_dict['streams'][0]['tags']['rotate']}")
        else:
            print(f"    >> No video Rotation needed...")
    else:
        print(f"    >> Video metadata not found...")

    return rotateCode


def overlay_image_alpha(img, img_overlay, pos):
    """Overlay img_overlay on top of img at the position specified by
    pos and blend using alpha_mask.

    Alpha mask must contain values within the range [0, 1] and be the
    same size as img_overlay.
    """

    x, y = pos

    # Image ranges
    y1, y2 = max(0, y), min(img.shape[0], y + img_overlay.shape[0])
    x1, x2 = max(0, x), min(img.shape[1], x + img_overlay.shape[1])

    # Overlay ranges
    y1o, y2o = max(0, -y), min(img_overlay.shape[0], img.shape[0] - y)
    x1o, x2o = max(0, -x), min(img_overlay.shape[1], img.shape[1] - x)

    # Exit if nothing to do
    if y1 >= y2 or x1 >= x2 or y1o >= y2o or x1o >= x2o:
        return

    channels = img.shape[2]

    # alpha = alpha_mask[y1o:y2o, x1o:x2o]
    # alpha_inv = 1.0 - alpha

    for c in range(channels):
        img[y1:y2, x1:x2, c] = img_overlay[y1o:y2o, x1o:x2o, c]


def interweavingMerge(array_a, array_b):
    merged_array = np.empty((array_a.size + array_b.size,), dtype=array_a.dtype)
    merged_array[0::2] = array_a
    merged_array[1::2] = array_b
    return merged_array


class Face2PPG:

    def __init__(self, face_detector, landmarks_detector, skin_segmentation, save_video, show_output, format_results,
                 save_results, ppg_method, fps=30):
        self.frameRate = fps
        self.postfiltering = True  # Filter POS signal after converting
        self.save_video = save_video
        self.show_output = show_output
        self.format_results = format_results
        self.save_results = save_results
        self.skinThresh_fix = [40, 60]
        self.ppg_method = ppg_method

        ##########################################
        # Detectors settings and initializations
        ##########################################

        # Default method for face detector "DNN_TF_OCV" ( DNN TensorFlow int8 model)
        self.faceDetector = FaceDetector(fd_method=face_detector)

        # Default method for face landmarks detector "OpenCV LBF"
        self.faceLandmarksDetector = FaceLandmarksDetector(fld_method=landmarks_detector)

        # Skin detector
        self.detectorSkin = skinDetector(fss_method=skin_segmentation)

    def extract_ppg_from_face(self, video_path):
        # Starting point
        video = video_path.split('/')
        print("    >> Starting to extract rPPG from video: " + str(video[-1]))

        # For saving results in HDF5. TimeStamp. Note: It could be taken before saving the results.
        dateTime = datetime.now()
        timestampStr = dateTime.strftime("%d-%b-%Y_(%H:%M:%S.%f)")

        # Record data video. Containers for saving the information. Note: More efficient containers??
        df_signals = pd.DataFrame()
        face_detected = []
        frame_number = []
        r_raw_signal = []
        g_raw_signal = []
        b_raw_signal = []

        number_of_faces = []

        # Video Reader
        cap = cv2.VideoCapture(video_path)
        hasFrame, frame = cap.read()
        if not hasFrame:
            exit(-1)

        rotate_code = None
        rotate_code = check_rotation(video_path)

        main_height, main_width, channels_main = frame.shape
        recording_fps = cap.get(cv2.CAP_PROP_FPS)
        length_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

        fs = recording_fps
        # Video Recorder.
        if self.save_video:
            vid_writer = cv2.VideoWriter('../data/videos/output/output-fba-{}.mp4'.format(str(video[-1]).split(".")[0]),
                                         cv2.VideoWriter_fourcc(*"mp4v"), int(20), (int(main_width * 2), main_height))

        ##########################################
        # Detectors settings and initializations
        ##########################################
        # Landmark detector
        self.faceLandmarksDetector.setParameters(width=main_width, height=main_height, image_data_type=frame.dtype)

        # Filtering
        filtering = Filtering(fps=fs)

        # Color method
        if self.ppg_method == "POS":
            colorTransformation = pos_method(fps=fs)
        elif self.ppg_method == "CHROM":
            colorTransformation = chrom_method(fps=fs)
        elif self.ppg_method == "LGI":
            colorTransformation = lgi_method(fps=fs)
        elif self.ppg_method == "OMIT":
            colorTransformation = omit_method(fps=fs)
        else:
            colorTransformation = pos_method(fps=fs)

        # Create a plotter class object. TODO: It needs be arranged according to the input resolution.
        if main_width > main_height:
            plot_width = main_width // 2
            plot_height = main_height // 4
            text_x_position = int(main_height * 0.02)
            text_y_position = int(main_height * 0.04)
        else:
            plot_width = main_height // 2
            plot_height = main_width // 4
            text_x_position = int(main_width * 0.02)
            text_y_position = int(main_width * 0.04)

        plotterRawPPG = RT2DPlotter(plot_width, plot_height, [1], number_of_signals=1, title='rPPG signal', index=1,
                                    axisLabels=['Seconds (s)', 'Amplitude'], colors=['blue'],
                                    signalLabels=['rPPG signal'], fps=fs)

        # General Variables
        frame_count = 0
        tt_FaceDetector = 0
        tt_FaceLandmarksDetector = 0
        tt_FaceSkinDetector = 0
        window_analysis = 3  # seconds

        # Visual settings
        if self.show_output:
            cv2.namedWindow("Video Output", cv2.WINDOW_NORMAL)
            cv2.startWindowThread()

        np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)

        #########################
        #       MAIN LOOP       #
        #########################

        while 1:

            if not hasFrame:
                break

            frame_number.append(frame_count)
            if rotate_code is not None:
                frame = cv2.rotate(frame, rotate_code)
                # frame = cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE)

            output_frame = frame.copy()  # Frame for drawing results
            cv2.putText(output_frame, "{}/{} processed frames.".format(frame_count, length_frames),
                        (text_x_position, text_y_position * 3), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        (0, 0, 255),
                        1, cv2.LINE_AA)

            # Face Detection
            t1 = time.time()
            bboxes = self.faceDetector.detectFaces(frame)
            number_of_faces.append(len(bboxes))
            tt_FaceDetector += time.time() - t1
            fpsFaceDetector = frame_count / tt_FaceDetector

            label = self.faceDetector.getLabelFaceDetector(fpsFaceDetector)
            cv2.putText(output_frame, label, (text_x_position, text_y_position), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        (0, 0, 255), 1, cv2.LINE_AA)

            # Print face rectangles
            getFirstFace = True  # Temporal. Get only first face
            for box in bboxes:
                if getFirstFace:
                    cv2.rectangle(output_frame, (box[0], box[1], box[2], box[3]), (0, 255, 0),
                                  int(round(output_frame.shape[0] * 0.001)))
                    faceRectImage, adaptedFaceRect = self.faceDetector.getFaceInRect(frame, box)
                    face_detected.append(int(1))
                    getFirstFace = False

            # Face Landmarks detection
            if len(bboxes) > 0:
                t2 = time.time()
                average_landmarks = np.zeros((68, 2))
                average_landmarks_factor = 1
                for i in range(average_landmarks_factor):
                    status, landmarks_s = self.faceLandmarksDetector.detectLandmarks(frame, bboxes)
                    average_landmarks = average_landmarks + landmarks_s[0][0]
                average_landmarks = average_landmarks / average_landmarks_factor

                landmarks = np.array([average_landmarks])

                tt_FaceLandmarksDetector += time.time() - t2
                fpsFaceLandmarksDetector = frame_count / tt_FaceLandmarksDetector
                label = self.faceLandmarksDetector.getLabelFaceLandmarksDetector(fpsFaceLandmarksDetector)
                cv2.putText(output_frame, label, (text_x_position, text_y_position * 2), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                            (0, 0, 255), 1, cv2.LINE_AA)

                # Compute landmarks for every face rects detected.
                getFirstFace = True  # Temporal. Get only first face
                for f in range(len(landmarks)):
                    if getFirstFace:
                        lm_x, lm_y = self.faceLandmarksDetector.transformLandmars2arrays(
                            f)  # Internally it uses landmarks[f]
                        landmarks85 = self.faceLandmarksDetector.get85Landmarks(frame, f)

                        # If we assume that it can be more faces than one. Later discuss
                        if len(landmarks) > 1:
                            number_of_faces.append(len(bboxes))
                            # frame_number.append(frame_count)
                        # cv2.face.drawFacemarks(output_frame, landmarks[f])
                        cv2.face.drawFacemarks(output_frame, np.array([landmarks85[f]]), color=[255, 255, 255])

                        if self.detectorSkin.fss_method == 'uNET':
                            t3 = time.time()
                            im_rgb = cv2.cvtColor(faceRectImage, cv2.COLOR_BGR2RGB)
                            skin_mask_bw = self.detectorSkin.find_skin(im_rgb)

                            hsm, wsm = skin_mask_bw.shape
                            # Pass to color
                            skin_mask = np.zeros((hsm, wsm, 3), skin_mask_bw.dtype)
                            skin_mask[:, :, 0] = skin_mask_bw
                            skin_mask[:, :, 1] = skin_mask_bw
                            skin_mask[:, :, 2] = skin_mask_bw

                            # locs = np.where(skin_mask_bw == 255)
                            # pixels = faceRectImage[locs]
                            skinFace = cv2.bitwise_or(faceRectImage, faceRectImage, mask=skin_mask_bw)
                            norMask = cv2.cvtColor(skinFace, cv2.COLOR_RGB2GRAY)
                            _, norMaskThres = cv2.threshold(norMask, thresh=2, maxval=255, type=cv2.THRESH_BINARY)

                            locs = np.where(skinFace > 0)
                            pixels = skinFace[locs]
                            avg_color = np.average(pixels)

                            mean_pixels = cv2.mean(skinFace, mask=norMaskThres)

                            tt_FaceSkinDetector += time.time() - t3
                            fpsFaceSkinDetector = frame_count / tt_FaceSkinDetector
                            label = self.detectorSkin.getLabelFaceSkinDetector(fpsFaceSkinDetector)
                            cv2.putText(output_frame, label, (text_x_position, text_y_position * 3),
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)

                        if self.detectorSkin.fss_method == 'COLOR':
                            assert len(self.skinThresh_fix) == 2, "Please provide 2 values for Fixed Skin Detector"
                            lower = np.array([0, self.skinThresh_fix[0], self.skinThresh_fix[1]], dtype="uint8")
                            upper = np.array([20, 255, 255], dtype="uint8")

                            converted = cv2.cvtColor(faceRectImage, cv2.COLOR_BGR2HSV)
                            skinMask = cv2.inRange(converted, lower, upper)
                            skinFace = cv2.bitwise_and(faceRectImage, faceRectImage, mask=skinMask)
                            mean_pixels = cv2.mean(skinFace, mask=skinMask)

                        # mean_pixels = np.asarray(mean_pixels)
                        r_raw_signal.append(mean_pixels[0])
                        g_raw_signal.append(mean_pixels[1])
                        b_raw_signal.append(mean_pixels[2])

                        getFirstFace = False

            else:
                print("  >> No Face Detected in frame " + str(frame_number) + "!!")
                face_detected.append(int(0))

            # Be careful. I am stamping the time also used for printing the results.
            # TODO: Flag for drawing the results or partitioned time measurement.
            time_consumed_for_processing_current_frame = time.time()

            # Plot in every frame the face and the skin mask.
            h, w, _ = output_frame.shape
            hl, wl, _ = skinFace.shape
            skinFace = cv2.resize(skinFace, (int(wl * 0.5), int(hl * 0.5)))
            hl, wl, _ = skinFace.shape
            starty = int(hl * 0.1)
            endy = hl - int(hl * 0.1)

            if self.show_output:
                # Provisional RT-PPG for visualization. Different signal to the final one with Butterworth filter.
                plotRawPPG = plotterRawPPG.plot(np.mean(mean_pixels))
                hp, wp, _ = plotRawPPG.shape
                plotRawPPG = cv2.resize(plotRawPPG, (int(wp * 0.5), int(hp * 0.5)))
                hp, wp, _ = plotRawPPG.shape
                overlay_image_alpha(output_frame, plotRawPPG,
                                    (int(main_height * 0.02), main_height - hp - int(main_height * 0.02)))
                # overlay_image_alpha(output_frame, skinFace, (text_x_position, text_y_position * 5))
                if self.detectorSkin.fss_method == 'uNET':
                    overlay_image_alpha(output_frame, skinFace, (w - wl - 2, 2))
                if self.detectorSkin.fss_method == 'COLOR':
                    overlay_image_alpha(output_frame, skinFace, (w - wl - 2, 2))

                cv2.imshow("Video Output", output_frame)

            # resized = cv2.resize(output_frame, (int(frame.shape[1]/4), int(frame.shape[0]/4)), interpolation = cv2.INTER_AREA)
            if self.save_video:
                vid_writer.write(output_frame)

            if frame_count == 1:
                tt_FaceDetector = 0

            if len(bboxes) > 0:
                frame_count += 1

            k = cv2.waitKey(1)
            if k == 27:
                break

            hasFrame, frame = cap.read()

        # Save Dataframe results.
        print("  >> Finalized to extract rPPG from video: " + str(video[-1]))
        print("     Shape frame array: {}".format(len(frame_number)))
        print("     Shape r_signal array: {}".format(len(r_raw_signal)))
        print("     Shape g_signal array: {}".format(len(g_raw_signal)))

        df_signals["Frame number"] = frame_number
        df_signals["Face detected"] = face_detected

        print("  >> Saving results of video " + str(video[-1]) + " with " + str(len(frame_number)) + " frames...")

        number_of_analysis_samples = window_analysis * fs
        RGBSignal = np.array([r_raw_signal, g_raw_signal, b_raw_signal])

        if self.postfiltering:
            ppg_signal = colorTransformation.apply(RGBSignal)
            ppg_signal = filtering.detrend(ppg_signal, method='Tarvainen')
            ppg_signal = filtering.BPfilter(ppg_signal, minHz=0.75, maxHz=4.0, order=6)

            # Just for showing the R channel filtered.
            RGBSignal[0] = filtering.detrend(RGBSignal[0], method='Tarvainen')
            RGBSignal[0] = filtering.BPfilter(RGBSignal[0], minHz=0.75, maxHz=4.0, order=6)
            RGBSignal[1] = filtering.detrend(RGBSignal[1], method='Tarvainen')
            RGBSignal[1] = filtering.BPfilter(RGBSignal[1], minHz=0.75, maxHz=4.0, order=6)
            RGBSignal[2] = filtering.detrend(RGBSignal[2], method='Tarvainen')
            RGBSignal[2] = filtering.BPfilter(RGBSignal[2], minHz=0.75, maxHz=4.0, order=6)
        else:
            RGBSignal[0] = filtering.detrend(RGBSignal[0], method='Tarvainen')
            RGBSignal[1] = filtering.detrend(RGBSignal[1], method='Tarvainen')
            RGBSignal[2] = filtering.detrend(RGBSignal[2], method='Tarvainen')
            RGBSignal = filtering.BPfilter(RGBSignal, minHz=0.75, maxHz=4.0, order=6)
            ppg_signal = colorTransformation.apply(RGBSignal)

        print("     Shape POS array: {}".format(len(ppg_signal)))
        print("     Shape RGB array: {}".format(RGBSignal.shape))
        if self.show_output:
            T1 = len(r_raw_signal) / fs  # duration, in seconds
            t1 = np.linspace(0, T1, len(r_raw_signal))
            import matplotlib.pyplot as plt
            plt.figure()

            plt.subplot(311)
            plt.plot(t1, g_raw_signal, 'g')
            plt.grid(b=True, which='major', color='#666666', linestyle='-')
            plt.legend(['raw_g_signal'], loc='upper right')
            plt.minorticks_on()
            plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
            plt.ylabel('amplitude')
            plt.title("Extracted green signal")

            plt.subplot(312)
            plt.plot(t1, RGBSignal[1], 'g')
            plt.grid(b=True, which='major', color='#666666', linestyle='-')
            plt.legend(['filtered_g_signal'], loc='upper right')
            plt.minorticks_on()
            plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
            plt.ylabel('amplitude')
            plt.title("Filtered green signal")

            plt.subplot(313)
            plt.plot(t1, ppg_signal, 'r')
            plt.grid(b=True, which='major', color='#666666', linestyle='-')
            plt.legend(['BVP_signal'], loc='upper right')
            plt.minorticks_on()
            plt.grid(b=True, which='minor', color='#999999', linestyle='-', alpha=0.2)
            plt.xlabel('time (s)')
            plt.ylabel('amplitude')
            plt.title(f"Extracted PPG signal with {self.ppg_method} method")

            plt.show()

        df_signals["PPG_signal"] = ppg_signal
        df_signals["raw_r_signal"] = r_raw_signal
        df_signals["raw_g_signal"] = g_raw_signal
        df_signals["raw_b_signal"] = b_raw_signal

        if self.save_results:
            output_path = video_path.replace(str(video[-1]), '')
            print(" >> Output path: " + output_path)

            if self.format_results == "HDF5":
                store_hdf5 = df_signals.HDFStore(output_path + str(video[-1].split(".")[0]) + '.h5')

            if self.format_results == "HDF5":
                store_hdf5.append(str(video[-1]).split(".")[0], df_signals)
            elif self.format_results == "CSV":
                df_signals.to_csv(output_path + str(video[-1].split(".")[0]) + '.csv', index=False, header=True)

        if self.show_output:
            cv2.destroyAllWindows()

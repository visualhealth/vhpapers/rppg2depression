# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Copyright (c) 2019 - 2022, Constantino Álvarez (CMVS - University of Oulu), Miguel
# Bordallo (VTT) and Face2PPG Contributors.
# All rights reserved.
#
# The full license is in the file LICENSE.txt, distributed with this software.
# -----------------------------------------------------------------------------


#############################################################################################################################################
#  USAGE:
#       - With folders: python ppg_extraction.py -f home/user_name/Projects/face2ppg_lite/data/videos --show True
#       - With video: python ppg_extraction.py -v ../data/videos/lgi_ppgi_cpi_resting.avi --show True --ppg CHROM
#  Depends of Face2PPG
#############################################################################################################################################

import os
import sys
from os.path import join
import glob

sys.path.insert(0, os.path.abspath('..'))
from face2ppg.face2ppg_lite import Face2PPG
from face2ppg.helpers.common import *


def main():
    ##########################
    # Input arguments parser #
    ##########################
    ap = argparse.ArgumentParser()

    ap.add_argument("-v", "--video", required=False, help="Path to input video")
    ap.add_argument("-f", "--folder", required=False, help="Path to file with list of input videos")
    ap.add_argument("-fd", "--facedetector", required=False, default="DNN_TF_OCV", help="Face Detector algorithm: DNN_TF_OCV, DNN_CAFFE_OCV,DLIB_HOG,... Default: DNN_TF_OCV")
    ap.add_argument("-fld", "--facelandmarksdetector", required=False, default="DLIB_ERT", help="Face Landmarks Detector algorithm: OCV_LBF, DLIB_ERT... Default: DLIB_ERT")
    ap.add_argument("-fss", "--faceskinsegmentation", required=False, default="COLOR", help="Face Skin Segmentation algorithm: uNET, COLOR... Default: COLOR")
    ap.add_argument("-ppg", "--ppg_method", required=False, default="CHROM", help="PPG method to transform the RGB signal to PPG: POS, CHROM, LGI, OMIT... Default: CHROM")
    ap.add_argument("-c", "--confidence", type=float, default=0.7, help="Minimum probability to filter weak detections. . Default: 0.7")
    ap.add_argument("-sv", "--save_videos", type=bool, default=False, required=False, help="Flag for saving the output videos. Default path: /data/videos/output/. Default: False")
    ap.add_argument("-sr", "--save_results", type=bool, default=True, required=False, help="Flag for saving the results in CSV by default. Default path: /path/to/input/videos. Default: True")
    ap.add_argument("-so", "--show", type=str2bool, nargs='?', const=True, default=False, required=False, help="Flag for showing the results while videos are processed. Default: False")
    ap.add_argument("-fr", "--format_results", default="CSV", required=False, help="Which format will be used to save the results. Options HDF5 or CSV. Default CSV")

    args = vars(ap.parse_args())

    face2ppg_converter = Face2PPG(face_detector=args["facedetector"], landmarks_detector=args["facelandmarksdetector"], skin_segmentation=args["faceskinsegmentation"],
                                  save_video=args["save_videos"], show_output=args["show"], format_results=args["format_results"], save_results=args["save_results"],
                                  ppg_method=args["ppg_method"])

    ###########################
    # Reading input arguments #
    ###########################
    # Input data. For our data.
    if args["video"] is not None:
        face2ppg_converter.extract_ppg_from_face(args["video"])
    elif args["folder"] is not None:
        videos = []
        for ext in ('*.avi', '*.mp4', '*.mpeg', '*.mpg'):
            videos.extend(glob.glob(join(args["folder"], ext)))

        print("  >> Number of found videos in the input path: {}".format(len(videos)))
        print("  >> Starting to process videos...\n")
        if videos:
            for video_path in videos:
                face2ppg_converter.extract_ppg_from_face(video_path)
        else:
            print("The input folder passed through arguments is empty...")
    else:
        videos = []
        for ext in ('*.avi', '*.mp4', '*.mpeg', '*.mpg'):
            videos.extend(glob.glob("../data/videos", ext))
        if videos:
            for video_path in videos:
                face2ppg_converter.extract_ppg_from_face(video_path)
        else:
            print("The input default folder passed through arguments is empty...")

    print("Work done!")


if __name__ == "__main__":
    main()

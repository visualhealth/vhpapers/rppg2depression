# -*- coding: utf-8 -*-

# -----------------------------------------------------------------------------
# Copyright (c) 2019 - 2022, Constantino Álvarez (CMVS - University of Oulu), Miguel
# Bordallo (VTT) and Face2PPG Contributors.
# All rights reserved.
#
# The full license is in the file LICENSE.txt, distributed with this software.
# -----------------------------------------------------------------------------


#############################################################################################################################################
#  USAGE:
#       - With folders: python ppg_extraction.py -f home/arritmic/Projects/GitLab/VisualHealth/RnD/Face2PPG/algo/videosss --show True
#       - With video: python ppg_extraction.py -v /home/arritmic/Projects/GitLab/VisualHealth/RnD/Face2PPG/algo/videosss/Tino_hr50_01.mp4 --show True -fld DAN
#  Depends of Face2PPG
#############################################################################################################################################

import os
import sys
from os.path import join
import glob
from pypapi import events, papi_high as high

sys.path.insert(0, os.path.abspath('..'))
from src.face2ppg_lite.face2ppg.face2ppg_lite import Face2PPG
from src.face2ppg_lite.face2ppg.helpers.common import *


def main(input_folder):
    ##########################
    # Input arguments parser #
    ##########################
    ap = argparse.ArgumentParser()

    ap.add_argument("-v", "--video", required=False, help="Path to input video")
    ap.add_argument("-f", "--folder", required=False, help="Path to file with list of input videos")
    ap.add_argument("-fd", "--facedetector", required=False, default="DNN_TF_OCV", help="Face Detector algorithm: DNN_TF_OCV, DNN_CAFFE_OCV,DLIB_HOG,... Default: DNN_TF_OCV")
    ap.add_argument("-fld", "--facelandmarksdetector", required=False, default="DAN", help="Face Landmarks Detector algorithm: OCV_LBF, DLIB_ERT, FAN, DAN... Default: DAN")
    ap.add_argument("-fss", "--faceskinsegmentation", required=False, default="LANDMARKS", help="Face Skin Segmentation algorithm: uNET, LANDMARKS, COLOR... Default: LANDMARKS")
    ap.add_argument("-c", "--confidence", type=float, default=0.7, help="Minimum probability to filter weak detections. . Default: 0.7")
    ap.add_argument("-tn", "--triangles_number", type=int, default=131, help="Number of triangles used for computing the normalized face. 17 or 131")
    ap.add_argument("-sv", "--save_videos", type=bool, default=False, required=False, help="Flag for saving the output videos. Default path: /data/videos/output/. Default: False")
    ap.add_argument("-sr", "--save_results", type=bool, default=True, required=False, help="Flag for saving the results in CSV by default. Default path: /path/to/input/videos. Default: True")
    ap.add_argument("-so", "--show", type=str2bool, nargs='?', const=True, default=False, required=False, help="Flag for showing the results while videos are processed. Default: False")
    ap.add_argument("-fr", "--format_results", default="CSV", required=False, help="Which format will be used to save the results. Options HDF5 or CSV. Default CSV")

    args = vars(ap.parse_args())

    face2ppg_converter = Face2PPG(face_detector=args["facedetector"], landmarks_detector=args["facelandmarksdetector"], triangles_number=args["triangles_number"],
                                  skin_segmentation=args["faceskinsegmentation"], save_video=args["save_videos"], show_output=args["show"], format_results=args["format_results"], save_results=args["save_results"])

    args["folder"] = input_folder
    ###########################
    # Reading input arguments #
    ###########################
    # Input data. For our data.
    t1 = time.time()
    if args["video"] is not None:
        face2ppg_converter.extract_ppg_from_face(args["video"])
    elif args["folder"] is not None:
        videos = []
        for ext in ('*.avi', '*.mp4', '*.mpeg', '*.mpg'):
            videos.extend(glob.glob(join(args["folder"], ext)))

        print("  >> Number of found videos in the input path: {}".format(len(videos)))
        print("  >> Starting to process videos...\n")
        if videos:
            # for video_path in videos:
            for i in range(len(videos)):
                video_path = videos[i]

                face2ppg_converter.extract_ppg_from_face(video_path)
                # if i == 0:
                #     face2ppg_converter.extract_ppg_from_face(video_path)
        else:
            print("The input folder passed through arguments is empty...")
    else:
        videos = []
        for ext in ('*.avi', '*.mp4', '*.mpeg', '*.mpg'):
            videos.extend(glob.glob("../data/videos", ext))
        if videos:
            for video_path in videos:
                face2ppg_converter.extract_ppg_from_face(video_path)
        else:
            print("The input default folder passed through arguments is empty...")


    print("Work done in {} seconds".format(time.time() - t1))


if __name__ == '__main__':
    # input arguments (Window size, slide window, device)
    main(sys.argv[1])

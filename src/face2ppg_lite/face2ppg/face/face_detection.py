# -*- coding: utf-8 -*-
"""
    Copyright (c) 2020 Constantino Álvarez Casado constantino.alvarezcasado@oulu.fi

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import division
import cv2
import dlib


class FaceDetector:
    ## Available face detectors.
    # OpenCV DNN supports 2 networks.
    # 1. FP16 version of the original caffe implementation ( 5.4 MB )
    # 2. 8 bit Quantized version using Tensorflow ( 2.7 MB )
    # 3. HOG Dlib.

    fd_method = "DNN_TF_OCV"
    conf_threshold = 0.7
    detector = []
    net = []

    def __init__(self, fd_method=None, conf_threshold=None):
        """
        Initialize the class with the given parameters.
        :param method: method used for face detection.
        :return:
        """
        if fd_method is not None:
            self.fd_method = fd_method
        if conf_threshold is not None:
            self.conf_threshold = conf_threshold

        if self.fd_method == "DNN_CAFFE_OCV":
            print("  >> Loading DNN_CAFFE_OCV model for face detection...")
            modelFile = "models/opencv/res10_300x300_ssd_iter_140000_fp16.caffemodel"
            configFile = "models/opencv/deploy.prototxt"
            self.detector = cv2.dnn.readNetFromCaffe(configFile, modelFile)
        elif self.fd_method == "DLIB_HOG":
            print("  >> Loading DLIB HOG for face detection...")
            self.detector = dlib.get_frontal_face_detector()
        else:  # default Face Detector "DNN_TF_OCV"
            print("  >> Loading DNN_TF_OCV model for face detection...")
            modelFile = "../models/opencv/opencv_face_detector_uint8.pb"
            configFile = "../models/opencv/opencv_face_detector.pbtxt"
            self.detector = cv2.dnn.readNetFromTensorflow(modelFile, configFile)

    def detectFaces(self, frame):
        if self.fd_method == "DNN_CAFFE_OCV":
            boxes = self.__detectFaceOpenCVDnnTF(frame)
        elif self.fd_method == "DLIB_HOG":
            boxes = self.__detectFaceDlibHog(frame)
        else:
            boxes = self.__detectFaceOpenCVDnnTF(frame)

        return boxes

    def __detectFaceOpenCVDnnTF(self, frame):
        frameOpencv = frame.copy()
        frameHeight = frameOpencv.shape[0]
        frameWidth = frameOpencv.shape[1]
        blob = cv2.dnn.blobFromImage(frameOpencv, 1.0, (300, 300), [104, 117, 123], False, False)

        self.detector.setInput(blob)
        detections = self.detector.forward()
        bboxes = []
        for i in range(detections.shape[2]):
            confidence = detections[0, 0, i, 2]
            if confidence > self.conf_threshold:
                x1 = int(detections[0, 0, i, 3] * frameWidth)
                y1 = int(detections[0, 0, i, 4] * frameHeight)
                x2 = int(detections[0, 0, i, 5] * frameWidth)
                y2 = int(detections[0, 0, i, 6] * frameHeight)
                bboxes.append([x1, y1, x2 - x1, y2 - y1])
        return bboxes

    def __detectFaceDlibHog(self, frame, inHeight=300, inWidth=0):

        frameDlibHog = frame.copy()
        frameHeight = frameDlibHog.shape[0]
        frameWidth = frameDlibHog.shape[1]
        if not inWidth:
            inWidth = int((frameWidth / frameHeight) * inHeight)

        scaleHeight = frameHeight / inHeight
        scaleWidth = frameWidth / inWidth

        frameDlibHogSmall = cv2.resize(frameDlibHog, (inWidth, inHeight))
        frameDlibHogSmall = cv2.cvtColor(frameDlibHogSmall, cv2.COLOR_BGR2RGB)
        faceRects = self.detector(frameDlibHogSmall, 0)
        bboxes = []
        for faceRect in faceRects:
            cvRect = [int(faceRect.left() * scaleWidth), int(faceRect.top() * scaleHeight),
                      int(faceRect.right() * scaleWidth) - int(faceRect.left() * scaleWidth), int(faceRect.bottom() * scaleHeight) - int(faceRect.top() * scaleHeight)]
            bboxes.append(cvRect)
            # cv2.rectangle(frameDlibHog, (cvRect[0], cvRect[1]), (cvRect[2], cvRect[3]), (0, 255, 0), int(round(frameHeight / 150)), 4)
        return bboxes

    def getFaceInRect(self, image, box):
        if self.fd_method == "DLIB_HOG":
            print("HERE    DLIB HOG")
            adaptedWidth = box[2] * 1.2
            center_x = box[0] + box[2] / 2
            center_y = box[1] + box[3] / 2
            x1 = int(center_x - adaptedWidth / 2)
            x2 = int(center_x + adaptedWidth / 2)
            y1 = int(center_y - adaptedWidth / 1.5)
            y2 = int(center_y + adaptedWidth / 2)
            if x1 < 0:
                x1 = 0
            if x2 > image.shape[1]:
                x2 = image.shape[1]
            if y1 < 0:
                y1 = 0
            if y2 > image.shape[0]:
                y2 = image.shape[0]
            # y2 = box[1] + (x2 - x1)
            faceRectImage = image[y1:y2, x1:x2]
            adaptedFaceRect = [box[0], box[1], box[2], box[3]]
        else:
            adaptedWidth = box[2] * 1.3
            center_x = box[0] + box[2] / 2
            x1 = int(center_x - adaptedWidth / 2)
            x2 = int(center_x + adaptedWidth / 2)
            if x1 < 0:
                x1 = 0
            if x2 > image.shape[1]:
                x2 = image.shape[1]
            y2 = box[1] + box[3]
            faceRectImage = image[box[1]:y2, x1:x2]
            adaptedFaceRect = [x1, box[1], x2 - x1, box[3]]

        return faceRectImage, adaptedFaceRect

    def getLabelFaceDetector(self, time):
        label = ""
        if self.fd_method == "DNN_TF_OCV":
            label = "OpenCV DNN TF Face Detector - FPS : {:.2f}".format(time)
        elif self.fd_method == "DNN_CAFFE_OCV":
            label = "OpenCV DNN Caffe Face Detector - FPS : {:.2f}".format(time)
        elif self.fd_method == "DLIB_HOG":
            label = "DLIB HOG Face Detector - FPS : {:.2f}".format(time)
        else:
            label = "Face Detector - FPS : {:.2f}".format(time)
        return label

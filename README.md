# Depression Recognition using Remote Photoplethysmography from Facial Videos (rPPG2Depression)


[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Python 3.6](https://img.shields.io/badge/python-3.6+-green.svg)](https://www.python.org/downloads/release/python-360/)
[![Work in Progress](https://img.shields.io/badge/state-work%20in%20progress-red)](https://gitlab.com/visualhealth/vhpapers/real-time-facealignment)


This is the official repository of "Depression Recognition using Remote Photoplethysmography from Facial Videos" paper.  The repository will contain soon a reference implementation and a set of models to regress a level of depression. This is a public repository intended for the general public.

If you use our models or implementation, please cite our journal article (currently under review): 

Álvarez Casado C., Lage Cañellas M., and Bordallo López M. "Depression Recognition using Remote Photoplethysmography from Facial Videos" (2022) 
[Download PDF](https://arxiv.org/abs/2206.04399) 

Authors:
- Constantino Álvarez Casado
- Manuel Lage Cañellas
- Miguel Bordallo López


![cover](documentation/images/user_interface_rPPG2Depression.png)





## Installation

If you want to use our test and evaluation application, clone the repository and follow the instructions.

## Requirements

* Python 3.6+
* Linux and Windows
* See also requirements.txt and environment.yml files

## Script for installation
For the installation of the _requirements_ and _environment_ run the next script:
* Run: `./install.sh`

If you are under Windows, use directly the _environment.yml_ file to install the 
dependencies.


<a name="myfootnote1">1</a>: *Not tested regularly on Windows. Fully tested in Linux Ubuntu O.S.*

## Usage
By default, the application read the videos from: `/data/videos`

By default, the results are saved in CSV format in: `/data/videos`.

Please, check the input arguments options in the _main_ functions of each python script or running
`python ./scripts/depression_regression.py --help`

![cover](documentation/images/front_depression_estimation_rPPG.jpg)

## Useful Resources and links of some parts of the code:
- https://github.com/MarekKowalski/DeepAlignmentNetwork
- https://gitlab.com/visualhealth/vhpapers/real-time-facealignment
- https://github.com/phuselab/pyVHR
- https://learnopencv.com/face-detection-opencv-dlib-and-deep-learning-c-python/
- http://dlib.net/


## Citation

If you use this software in your research, then please cite the following:

Álvarez Casado C., Lage Cañellas M., and Bordallo López M. "Depression Recognition using Remote Photoplethysmography from Facial Videos" (2022) 
[Download PDF](https://arxiv.org/abs/2206.04399) 

## License

This project is licensed under the MIT License - see the LICENSE.md file for details

## Disclaimer
*The authors do not provide any warranty. If this software causes your computer explodes, blow your mind away, your toilet to clog or your pet to barf in the carpet, the authors CANNOT IN ANY WAY be held responsible.*
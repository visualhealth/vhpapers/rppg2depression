#!/usr/bin/python

import sys
import ppg_extraction

print('*****************************')
print('*       BATCH TASK 1        *')
print('*****************************')
ppg_extraction.main("/media/arritmic/ADATA_001/DATABASES/DEPRESSION/Depression_Datasets/AVEC2014/Training/Freeform")
print(" ######################## TASK Training/Freeform DONE!! ###########################")


print('*****************************')
print('*       BATCH TASK 2        *')
print('*****************************')
ppg_extraction.main("/media/arritmic/ADATA_001/DATABASES/DEPRESSION/Depression_Datasets/AVEC2014/Development/Freeform")
print(" ######################## TASK Development/Freeform DONE!! ###########################")


print('*****************************')
print('*       BATCH TASK 3        *')
print('*****************************')
ppg_extraction.main("/media/arritmic/ADATA_001/DATABASES/DEPRESSION/Depression_Datasets/AVEC2014/Testing/Freeform")
print(" ######################## TASK Testing/Freeform DONE!! ###########################")


print('*****************************')
print('*       BATCH TASK 4        *')
print('*****************************')
ppg_extraction.main("/media/arritmic/ADATA_001/DATABASES/DEPRESSION/Depression_Datasets/AVEC2014/Training/Northwind")
print(" ######################## TASK Training/Northwind DONE!! ###########################")


print('*****************************')
print('*       BATCH TASK 5        *')
print('*****************************')
ppg_extraction.main("/media/arritmic/ADATA_001/DATABASES/DEPRESSION/Depression_Datasets/AVEC2014/Development/Northwind")
print(" ######################## TASK Development/Northwind DONE!! ###########################")


print('*****************************')
print('*       BATCH TASK 6        *')
print('*****************************')
ppg_extraction.main("/media/arritmic/ADATA_001/DATABASES/DEPRESSION/Depression_Datasets/AVEC2014/Testing/Northwind")
print(" ######################## TASK Testing/Northwind DONE!! ###########################")


# print('*****************************')
# print('*       BATCH TASK 7        *')
# print('*****************************')
# ppg_extraction.main("/media/arritmic/ADATA_002/DATABASES/DEPRESSION/AVEC13/AVEC2013/Training_AudioVisual/Training")
# print(" ######################## TASK Training_AudioVisual/Training DONE!! ###########################")
#
#
# print('*****************************')
# print('*       BATCH TASK 8        *')
# print('*****************************')
# ppg_extraction.main("/media/arritmic/ADATA_002/DATABASES/DEPRESSION/AVEC13/AVEC2013/Testing_AudioVisual/Testing")
# print(" ######################## TASK Testing_AudioVisual/Testing DONE!! ###########################")


# print('*****************************')
# print('*       BATCH TASK 9        *')
# print('*****************************')
# ppg_extraction.main("/media/arritmic/ADATA_002/DATABASES/DEPRESSION/AVEC13/AVEC2013/Development_AudioVisual/Development")
# print(" ######################## TASK Development_AudioVisual/Development DONE!! ###########################")
#

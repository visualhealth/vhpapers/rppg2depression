from scipy import signal
import numpy as np

# Function inspired by https://github.com/phuselab/pyVHR

class chrom_method:
    """ This method is described in the following paper:
        "Remote heart rate variability for emotional state monitoring"
        by Y. Benezeth, P. Li, R. Macwan, K. Nakamura, R. Gomez, F. Yang
    """
    methodName = 'CHROM'

    def __init__(self, fps):
        self.frameRate = fps

    def apply(self, X):

        # calculation of new X and Y
        Xcomp = 3 * X[0] - 2 * X[1]
        Ycomp = (1.5 * X[0]) + X[1] - (1.5 * X[2])

        # standard deviations
        sX = np.std(Xcomp)
        sY = np.std(Ycomp)

        if sY != 0.0:
            alpha = sX / sY
        else:
            alpha = 1.0

        # -- rPPG signal
        bvp = Xcomp - alpha * Ycomp

        return bvp


# -*- coding: utf-8 -*-
"""
    Copyright (c) 2020 Constantino Álvarez Casado constantino.alvarezcasado@oulu.fi

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from __future__ import division
import cv2
import warnings
import numpy as np
import face_alignment
import dlib
from imutils import face_utils
import copy


def faceBoxCorrection(face_box):
    # Para transformar de rectángulo a cuadrado. Para las face DNNs
    x_center = face_box[0] + face_box[2] / 2
    y_center = face_box[1] + face_box[3] / 2

    face_box[0] = x_center - 1.3 * face_box[2] / 2
    face_box[1] = y_center - 1.3 * face_box[2] / 2
    face_box[3] = 1.3 * face_box[2]
    face_box[2] = face_box[3]

    return face_box

def faceBoxCorrectionForFAN(face_box):
    # Corrección para la API de Bullat.
    face_box[2] = face_box[0] + face_box[2]
    face_box[3] = face_box[1] + face_box[3]

    return face_box


def create_blank(width, height, rgb_color=(0, 0, 0)):
    """Create new image(numpy array) filled with certain color in RGB"""
    # Create black blank image
    image = np.zeros((height, width, 3), np.uint8)

    # Since OpenCV uses BGR, convert the color first
    color = tuple(reversed(rgb_color))
    # Fill image with color
    image[:] = color
    return image


class FaceLandmarksDetector:

    ## Available face landmarks detectors.
    # 1. LBF OpenCV
    # 2. FAN Adrian Bulat
    # 3. DLIB ERT

    # Variables
    fld_method = "OCV_LBF"
    conf_threshold = 0.7
    detector = []
    landmarks = []

    def __init__(self, fld_method=None, conf_threshold=None):
        """
        Initialize the class with the given parameters.
        :param method: method used for face alignment.
        :return:
        """

        self.height = -1
        self.width = -1
        self.image_channels = -1
        self.image_data_type = -1
        self.frame_number = 0
        self.landmarks85 = []
        self.landmarks_x = []
        self.landmarks_y = []


        if fld_method is not None:
            self.fld_method = fld_method
        if conf_threshold is not None:
            self.conf_threshold = conf_threshold

        if fld_method == "DLIB_ERT":
            print("  >> Loading DLIB model for face landmarks detection...")
            modelFile = "../models/dlib/shape_predictor_68_face_landmarks_GTX.dat"
            self.detector = dlib.shape_predictor(modelFile)

        elif fld_method == "FAN":
            print("  >> Loading FAN model for face landmarks detection...")
            self.detector = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, flip_input=False, device="cpu")

        else:
            print("  >> Loading LBF model for face landmarks detection...")
            modelFile = "../models/dlib/LBF686_GTX.yaml"
            # TODO: Modified Opencv C++ code for making accesible the struct Params to silent of the verbose flag.
            self.detector = cv2.face.createFacemarkLBF()
            self.detector.loadModel(modelFile)


    def setParameters(self, width, height, image_data_type, image_channels=3):

        self.height = height
        self.width = width
        self.image_channels = image_channels
        self.image_data_type = image_data_type
        self.frame_number = 0


    def detectLandmarks(self, frame, in_boxes):

        # boxes = in_boxes.copy()
        boxes = copy.deepcopy(in_boxes)

        for n in range(len(boxes)):
            boxes[n] = faceBoxCorrection(boxes[n])
            break  # ToDo: Track face. Select bigger face


        if self.fld_method == "OCV_LBF":
            status, landmarks = self.detector.fit(frame, np.array(boxes))

        elif self.fld_method == "FAN":
            for n in range(len(boxes)):
                boxes[n] = faceBoxCorrectionForFAN(boxes[n])  # Change coordinates.
            landmarks = self.detector.get_landmarks(frame, detected_faces=np.array(boxes))
            status = "okay"
            # Convertion for adapting the output to the OpenCV format.
            after = [np.array([x]) for x in landmarks]
            landmarks = after

        elif self.fld_method == "DLIB_ERT":
            # determine the facial landmarks for the face region, then
            # convert the facial landmark (x, y)-coordinates to a NumPy
            # array
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            landmarks = []
            status = "okay"
            for box in boxes:
                shape = self.detector(gray, dlib.rectangle(int(box[0]), int(box[1]), int(box[0] + box[2]),
                                                           int(box[1] + box[3])))
                shape = face_utils.shape_to_np(shape)
                landmarks.append(shape)
            after = [np.array([x]) for x in landmarks]
            landmarks = after

        self.landmarks = landmarks
        self.landmarks85 = self.landmarks.copy()

        return status, landmarks


    def transformLandmars2arrays(self, index):

        temp = self.landmarks[index].transpose()
        self.landmarks_x = temp[0].transpose()[0]
        self.landmarks_y = temp[1].transpose()[0]
        return self.landmarks_x, self.landmarks_y



    def get85Landmarks(self, image, index):

        # Hacer aquí la copia no vale, porque se sobreescribirían los resultados.
        # La copia es hecha cuándo se calculan los 68 puntos.
        # self.landmarks82 = self.landmarks.copy()

        # Time for computing strategic points.

        # TODO: Revisar los vectores "self.landmarks_x" y "self.landmarks_y".
        # Ahora mismo son los datos de la cara que se está analizando.
        # point = (self.landmarks_x[48],self.landmarks_y[48])
        # print(point[0])
        # print(point[1])

        # Computing strategic points
        it = self.createLineIterator((self.landmarks_x[36], self.landmarks_y[36]),
                                     (self.landmarks_x[31], self.landmarks_y[31]), image)
        central_left = (it[0][0:2] + it[-1][0:2]) / 2
        # central_left_np = np.array(central_left).reshape(1,1,2)

        # Nose --> face [Left side]
        it = self.createLineIterator((self.landmarks_x[31], self.landmarks_y[31]),
                                     (self.landmarks_x[3], self.landmarks_y[3]), image)
        # it = self.createLineIterator(central_left,(self.landmarks_x[2], self.landmarks_y[2]), image)
        cheek_left = (it[0][0:2] + it[-1][0:2]) / 2
        # cheek_left_np = np.array(cheek_left).reshape(1, 1, 2)

        # Nose --> face [Left side]
        it = self.createLineIterator((self.landmarks_x[36], self.landmarks_y[36]),
                                     (self.landmarks_x[2], self.landmarks_y[2]), image)
        # it = self.createLineIterator(central_left,(self.landmarks_x[2], self.landmarks_y[2]), image)
        cheek_left2 = (it[0][0:2] + it[-1][0:2]) / 2
        # cheek_left_np = np.array(cheek_left).reshape(1, 1, 2)

        # Mouth --> eyes[Right side]
        it = self.createLineIterator((self.landmarks_x[35], self.landmarks_y[35]),
                                     (self.landmarks_x[45], self.landmarks_y[45]), image)
        central_right = (it[0][0:2] + it[-1][0:2]) / 2
        # central_right_np = np.array(central_right).reshape(1, 1, 2)

        # Nose --> face [Right side]
        it = self.createLineIterator((self.landmarks_x[35], self.landmarks_y[35]),
                                     (self.landmarks_x[13], self.landmarks_y[13]), image)
        # it = self.createLineIterator(central_right,(self.landmarks_x[14], self.landmarks_y[14]), image)
        cheek_right = (it[0][0:2] + it[-1][0:2]) / 2
        # cheek_right_np = np.array(cheek_right).reshape(1, 1, 2)

        # Nose --> face [Right side]
        it = self.createLineIterator((self.landmarks_x[45], self.landmarks_y[45]),
                                     (self.landmarks_x[14], self.landmarks_y[14]), image)
        # it = self.createLineIterator(central_right,(self.landmarks_x[14], self.landmarks_y[14]), image)
        cheek_right2 = (it[0][0:2] + it[-1][0:2]) / 2
        # cheek_right_np = np.array(cheek_right).reshape(1, 1, 2)

        # Add new points to the shape face
        # self.landmarks82[index] = np.vstack((self.landmarks82[index][0], np.array([cheek_left])))
        # print(self.landmarks85[index].shape)
        self.landmarks85[index] = np.vstack((self.landmarks85[index][0], np.array([central_left])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([cheek_left])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([cheek_left2])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([central_right])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([cheek_right])))
        self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([cheek_right2])))

        # print( self.landmarks85[index].shape)
        # self.landmarks82[index].append(cheek_left)
        # self.landmarks82[index].append(central_left)
        # self.landmarks82[index].append(cheek_right)
        # self.landmarks82[index].append(central_right)

        # Add extra points forehead
        distanceNosePointsY = (self.landmarks_y[29] - self.landmarks_y[27]) * 1.5

        extra_points = 0
        for i in range(17, 27):
            if 19 <= i <= 24:
                if i == 19:
                    temp_point = (self.landmarks_x[i], self.landmarks_y[i] - distanceNosePointsY - distanceNosePointsY*0.05)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 20:
                    temp_point = (self.landmarks_x[i], self.landmarks_y[19] - distanceNosePointsY - distanceNosePointsY*0.1)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 21:
                    temp_point = (self.landmarks_x[i], self.landmarks_y[19] - distanceNosePointsY - distanceNosePointsY*0.13)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 21:
                    temp_point = (self.landmarks_x[27], self.landmarks_y[19] - distanceNosePointsY - distanceNosePointsY*0.15)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 22:
                    temp_point = (self.landmarks_x[i], self.landmarks_y[19] - distanceNosePointsY - distanceNosePointsY*0.13)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 23:
                    temp_point = (self.landmarks_x[i], self.landmarks_y[19] - distanceNosePointsY - distanceNosePointsY*0.1)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

                if i == 24:
                    temp_point = (self.landmarks_x[i], self.landmarks_y[i] - distanceNosePointsY - distanceNosePointsY*0.05)
                    self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))
            else:
                temp_point = (self.landmarks_x[i], self.landmarks_y[i] - distanceNosePointsY)
                self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))
            # Central point over the eyebrowns
            # if i == 21:
            #     temp_point = (
            #     self.landmarks_x[27], self.landmarks_y[27] - distanceNosePointsY - distanceNosePointsY / 2)
            #     self.landmarks85[index] = np.vstack((self.landmarks85[index], np.array([temp_point])))

        return self.landmarks85



    def createLineIterator(self, P1, P2, img):
        """
        Produces and array that consists of the coordinates and intensities of each pixel in a line between two points
        Function inspired on: https://stackoverflow.com/questions/32328179/opencv-3-0-lineiterator

        Parameters:
            -P1: a numpy array that consists of the coordinate of the first point (x,y)
            -P2: a numpy array that consists of the coordinate of the second point (x,y)
            -img: the image being processed

        Returns:
            -it: a numpy array that consists of the coordinates and intensities of each pixel in the radii (shape: [numPixels, 3], row = [x,y,intensity])
        """

        # define local variables for readability

        imageH = img.shape[0]
        imageW = img.shape[1]
        P1X = P1[0]
        P1Y = P1[1]
        P2X = P2[0]
        P2Y = P2[1]
        # P1X = np.trunc(P1[0])
        # P1Y = np.trunc(P1[1])
        # P2X = np.trunc(P2[0])
        # P2Y = np.trunc(P2[1])

        # difference and absolute difference between points
        # used to calculate slope and relative location between points
        dX = P2X - P1X
        dY = P2Y - P1Y
        dXa = int(round(np.abs(dX)))
        dYa = int(round(np.abs(dY)))

        # predefine numpy array for output based on distance between points
        itbuffer = np.empty(shape=(np.maximum(int(dYa), int(dXa)), 3), dtype=np.float32)
        itbuffer.fill(np.nan)


        # Obtain coordinates along the line using a form of Bresenham's algorithm
        negY = P1Y > P2Y
        negX = P1X > P2X

        if P1X == P2X:  # vertical line segment
            itbuffer[:, 0] = P1X
            if negY:
                itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
            else:
                itbuffer[:, 1] = np.arange(P1Y + 1, P1Y + dYa + 1)
        elif P1Y == P2Y:  # horizontal line segment
            itbuffer[:, 1] = P1Y
            if negX:
                itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
            else:
                itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
        else:  # diagonal line segment
            steepSlope = dYa > dXa
            if steepSlope:
                slope = dX.astype(np.float32) / dY.astype(np.float32)
                if negY:
                    itbuffer[:, 1] = np.arange(P1Y - 1, P1Y - dYa - 1, -1)
                else:
                    itbuffer[:, 1] = np.arange(round(P1Y) + 1, round(P1Y) + dYa + 1)

                itbuffer[:, 0] = (slope * (itbuffer[:, 1] - P1Y)).astype(np.int) + P1X
            else:
                slope = dY.astype(np.float32) / dX.astype(np.float32)
                if negX:
                    itbuffer[:, 0] = np.arange(P1X - 1, P1X - dXa - 1, -1)
                else:
                    itbuffer[:, 0] = np.arange(P1X + 1, P1X + dXa + 1)
                itbuffer[:, 1] = (slope * (itbuffer[:, 0] - P1X)).astype(np.int) + P1Y

        # Remove points outside of image
        colX = itbuffer[:, 0]
        colY = itbuffer[:, 1]
        itbuffer = itbuffer[(colX >= 0) & (colY >= 0) & (colX < imageW) & (colY < imageH)]

        # Get intensities from img ndarray
        itbuffer[:, 2] = img[itbuffer[:, 1].astype(np.uint), itbuffer[:, 0].astype(np.uint), 2]

        return itbuffer


    def getLabelFaceLandmarksDetector(self, time):
        label = ""
        if self.fld_method == "OCV_LBF":
            label = "OpenCV LBF Face Landmarks Detector - FPS : {:.2f}".format(time)
        elif self.fld_method == "FAN":
            label = "FAN Face Landmarks Detector - FPS : {:.2f}".format(time)
        elif self.fld_method == "DLIB_ERT":
            label = "DLIB ERT Face Landmarks Detector - FPS : {:.2f}".format(time)
        else:
            abel = "Face Landmarks Detector - FPS : {:.2f}".format(time)
        return label

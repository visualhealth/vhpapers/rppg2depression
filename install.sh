#!/bin/bash

#1) Create and activate environment
ENVS=$(conda info --envs | awk '{print $1}' )
if [[ $ENVS = *"rPPG2Depression"* ]]; then
   source ~/anaconda3/etc/profile.d/conda.sh
   conda activate rPPG2Depression
else
   echo "Creating a new conda environment for rPPG2Depression project..."
   #conda env create -n Face2PPG python=3.8
   conda env create -f environment.yml
   source ~/anaconda3/etc/profile.d/conda.sh
   conda activate rPPG2Depression
   #python setup.py install
   #exit
fi;


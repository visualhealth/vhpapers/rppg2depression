import setuptools
import os

requirementPath = 'requirements.txt'
reqs = []
if os.path.isfile(requirementPath):
    with open(requirementPath) as f:
        reqs = f.read().splitlines()

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="rPPG2Depression",
    version="0.0.1",
    author="Constantino Álvarez Casado <constantino.lvarezcasado@oulu.fi>, Miguel Bordallo López <miguel.bordallo@oulu.fi>",
    author_email="",
    description="Package for detect Depression using rPPG signals from video faces.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/visualhealth/vhpapers/rppg2depression",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approvedconda  :: GNU License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=reqs
)
